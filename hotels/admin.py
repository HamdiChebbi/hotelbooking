from django.contrib import admin

from models import Hotel, Chambre

admin.site.register(Hotel)
admin.site.register(Chambre)
