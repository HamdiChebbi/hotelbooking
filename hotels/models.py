from __future__ import unicode_literals

from django.db import models

'''Classe de gestion des hotels'''


class Hotel(models.Model):
    nom = models.CharField(max_length=255)
    description = models.TextField()
    etoiles = models.PositiveSmallIntegerField()
    chambres = models.PositiveIntegerField()
    photo = models.ImageField(null=True)
    # prix = models.DecimalField()
    contact = models.EmailField()
    note = models.DecimalField(decimal_places=1, max_digits=5, null=True)
    date_creation = models.DateField(null=True)
    actif = models.BooleanField(default=True)

    def __str__(self):
        return self.nom

    '''Classe de gestion des chambres'''
    '''Hotel peut avoir plusieurs chambres'''

class Chambre(models.Model):
    numero = models.CharField(max_length=5)
    disponible = models.BooleanField()
    hotel = models.ForeignKey(Hotel)
