from django.shortcuts import render
from models import Hotel


# crer une methode
def index(request):
    #hotels = Hotel.objects.filter( etoiles__gt=2, description__contains='nice').order_by('nom')
    hotels = Hotel.objects.all()
    data = {
        'hotels': hotels,
        'title': 'Liste des hotels',
    }
    return render(request, 'index.html', data)

# Create your views here.
